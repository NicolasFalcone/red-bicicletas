var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
var mongoose = require('mongoose');

describe('Bicicleta API', () => {
    beforeEach(function(){
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function(){
            console.log('Conectados a la base de datos!');
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if(err) 
                console.log(err);
            done();
        });
    });
    
    describe ('GET BICICLETAS /', () => {
        it('Status 200 vacia', (done) => {
            request.get('http://localhost:5000/api/bicicletas', function(error, response, body){
                var result = JSON.parse(body).bicicletas;
                expect(response.statusCode).toBe(200);
                expect(result.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"code": 10, "color": "Rojo", "modelo": "Urbana", "lat":-34, "lng": -54}';
            
            request.post({
                headers: headers,
                url: 'http://localhost:5000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("Rojo");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            });
        });
    });

    describe('DELETE BICICLETAS /delete', () => {
        it('Status 204', (done) => {
            var a = Bicicleta.createInstance(1, 'Negro', 'Urbana', [-34.6012424, -57.5570452]);
            Bicicleta.add(a, function(err, newBici){
                var headers = {'content-type' : 'application/json'};
                var aBici = '{"code": 1}';

                request.post({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/delete',
                    body: aBici
                }, function(error, response, body) {
                    expect(response.statusCode).toBe(204);
                    Bicicleta.allBicis(function(err, bicis){
                        expect(bicis.length).toBe(0);
                    });
                    done();
                });
            });
        });
    });

    describe('UPDATE BICICLETAS /update', () => {
        it('Status 200', (done) => {
            var a = Bicicleta.createInstance(1, 'Negro', 'Urbana', [-34.6012424, -57.5570452]);
            Bicicleta.add(a, function(err, newBici) {
                var headers = {'content-type' : 'application/json'};
                var aBici = '{"code": 1, "color": "Rojo", "modelo": "Montaña", "lat":-34, "lng": -54}';
                
                request.post({
                    headers: headers,
                    url: 'http://localhost:5000/api/bicicletas/update',
                    body: aBici
                }, function(error, response, body) {
                    expect(response.statusCode).toBe(200);
                    var bici = JSON.parse(body).bicicleta;
                    expect(bici.color).toBe("Rojo");
                    expect(bici.modelo).toBe("Montaña");
                    done();
                });
            });
        });
    });
});