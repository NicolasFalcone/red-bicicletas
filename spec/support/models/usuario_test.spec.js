var mongoose = require('mongoose');
var Bicicleta = require('../../../models/bicicleta');
var Usuario = require('../../../models/usuario');
var Reserva = require('../../../models/reserva');
var server = require('../../../bin/www');

describe('Testing Usuarios', function(){

    beforeEach(function(){
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function(){
            console.log('Conectados a la base de datos!');
        })
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success) {
            if(err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if(err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if(err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Usuario.reservar', () => {
        it('Debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre: 'Nicolas', email: 'hola@gmail.com', password: 'psw'});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate()+1);

            usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
})