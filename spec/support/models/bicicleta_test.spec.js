var mongoose = require('mongoose');
var Bicicleta = require('../../../models/bicicleta');
var server = require('../../../bin/www');

describe('Testing Bicicletas', function(){
    beforeEach(function(){
        var mongoDB = 'mongodb://localhost/red_bicicletas';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});

        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function(){
            console.log('Conectados a la base de datos!');
        })
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if(err) 
                console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "Verde", "Urbana", [-38.008525,-57.5570452]);
    
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("Verde");
            expect(bici.modelo).toBe("Urbana");
            expect(bici.ubicacion[0]).toBe(-38.008525);
            expect(bici.ubicacion[1]).toBe(-57.5570452);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza vacía', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicletas.add', () => {
        it('Agrega una bicicleta', (done) => {
            var bici = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
            Bicicleta.add(bici, function(err, newBici){
                if(err) 
                    console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(bici.code);

                    done();
                })
            })
        })
    })

    describe('Bicicletas.findByCode', () => {
        it('Devuelve la bicicleta con código 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var bici = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
                Bicicleta.add(bici, function(err, newBici){
                    if(err) 
                        console.log(err);

                    var bici2 = new Bicicleta({code: 2, color: "Roja", modelo: "Urbana"});
                    Bicicleta.add(bici2, function(err, newBici){
                        if(err) 
                            console.log(err);
                        Bicicleta.findByCode(1,function(error, targetBici){
                            expect(targetBici.code).toBe(bici.code);
                            expect(targetBici.color).toBe(bici.color);
                            expect(targetBici.modelo).toBe(bici.modelo);
        
                            done();
                        });
                    });
                });
            });  
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('Debe borrar la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana", ubicacion: [-34, -54]});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);

                    Bicicleta.add(aBici, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.allBicis(function(err, bicis){
                            expect(bicis.length).toBe(1);
                            Bicicleta.removeByCode(1, function(error, response) {
                                Bicicleta.allBicis(function(err, bicis){
                                    expect(bicis.length).toBe(0);
                                    done();
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});