var mymap = L.map('main_map').setView([-37.9963997,-57.547876], 14);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

/*
var marker = L.marker([-38.008525,-57.5570452]).addTo(mymap);
var marker = L.marker([-37.993336, -57.547745]).addTo(mymap);
var marker = L.marker([-37.9560217,-57.5502212]).addTo(mymap);*/

$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title:bici.id}).addTo(mymap);
        });
    }
})