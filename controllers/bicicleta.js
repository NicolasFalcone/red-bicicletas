var bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res){
    bicicleta.find({}, function(err, bicis){
        res.render('bicicletas/index', {bicis : bicis});
    });
}

exports.bicicleta_create_get = function(req, res, next){
    res.render('bicicletas/create', {errors: {}, bicicleta: new bicicleta()});
}

exports.bicicleta_create_post = function(req, res, next){
    bicicleta.create({color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]}, function(err, nuevaBicicleta){
        if(err)
            res.render('bicicletas/create', {errors: err.errors, bicicleta: new bicicleta({color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]})});
        else 
            res.redirect('/bicicletas');
    });
}

exports.bicicleta_update_get = function(req, res){
    var bici = bicicleta.findById(req.params.id, function(err, bici){
        res.render('bicicletas/update', {errors: {}, bici:bici});
    });
}

exports.bicicleta_update_post = function(req, res){
    var update_values = {code: req.body.code, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]};
    bicicleta.findByIdAndUpdate(req.params.id, update_values, function(err, bici){
        if(err) {
            console.log(err);
            res.render('bicicletas/update', {errors: err.errors, bici: new bicicleta({color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.lng]})});
        }
        else {
            res.redirect('/bicicletas');
            return;
        }
    });

}

exports.bicicleta_delete_post = function(req, res, next){
    bicicleta.findByIdAndDelete(req.params.id, function(err){
        if(err)
            next(err);
        else
            res.redirect('/bicicletas');
    });
}